
$(document).ready(function () {
    function requestPost(url,body,callback){
        var oAjaxReq = new XMLHttpRequest();
        oAjaxReq.open("post", url, true);
        oAjaxReq.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        oAjaxReq.send(body);
        oAjaxReq.onreadystatechange = callback;
    }

    function readCookie(name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for(var i=0;i < ca.length;i++) {
            var c = ca[i];
            while (c.charAt(0)==' ') c = c.substring(1,c.length);
            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
        }
        return null;
    }

    $(".main_menu_g").mouseenter(function() {
        $('.main_hov_li').hide();
        $(this).next('li').show();
    });
    $('.main_menu ').mouseleave(function() {
        $('.main_hov_li').hide();
    });
})


