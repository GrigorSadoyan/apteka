<?php

namespace administrator\Controller;
use administrator\Core\Controller as BaseController;
use Model\Categories as Categ;
class Categories extends BaseController
{
    public function __construct($route,$countRoute)
    {

        parent::__construct();

        if($_SERVER['REQUEST_METHOD'] == 'GET') {
            if ($countRoute == 1 && $route[0] == 'categories') {
                $this->index();
            }elseif ($countRoute == 2 && $route[0] == 'categories' && $route[1] == 'add') {
                $this->item();
            }elseif ($countRoute == 2 && $route[0] == 'categories' && is_numeric($route[1])) {
                $this->item($route[1]);
            }else{
                $this->renderNotFound('main');
                die();
            }
        }
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            if ($countRoute == 2 && $route[0] == 'categories' && $route[1] == 'add'){
                $this->addCategories();
            }elseif ($countRoute == 2 && $route[0] == 'categories' && is_numeric($route[1])) {
                $this->UpdateCategories($route[1]);
            }elseif ($countRoute == 2 && $route[0] == 'categories' && $route[1] == 'delete') {
                $this->DeleteCategories();
            }
        }
    }

    private function index(){
        $_oCategories = new Categ();
        $aCategories = $_oCategories->findAll(array('order'=>array('desc'=>'id')));
        $this->result['result']=$aCategories;
        $this->renderView("Pages/categories","categories",$this->result);
    }
    private function item($id = FALSE){
        if($id){
            $_oCategories = new Categ();
            $aCategories = $_oCategories->findById($id);
            $this->result['result']=$aCategories;
        }
        $this->renderView("Pages/categoriesinfo","categoriesinfo",$this->result);
    }
    private function addCategories(){
        $_oCategories = new Categ();
        $_oCategories->_post=$_POST;
        $lastId = $_oCategories->insert();
        $url = "categories";
        $this->headreUrl($url);
    }
    private function UpdateCategories($id){
        $_oCategories = new Categ();
        $_oCategories->_put = $_POST;
        $_oCategories->setId($id);
        $_oCategories->update();
        $url = "categories";
        $this->headreUrl($url);
    }
    private function DeleteCategories(){
        $id = $_POST['id'];
        $_oCategories = new Categ();
        $_oCategories->delFildName = 'id';
        $_oCategories->delValue = $id;
        $_oCategories->delete();
        echo json_encode(array('error'=>true));
    }
}