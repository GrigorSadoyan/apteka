<?php
/**
 * Created by PhpStorm.
 * User: sadoy
 * Date: 4/25/2018
 * Time: 2:15 PM
 */

namespace administrator\Controller;
use administrator\Core\Controller as BaseController;
use Model\Product as Prod;
use Model\Pets;
use Model\Categories;
use Model\Producttype;
class Product extends  BaseController
{
    public function __construct($route = FALSE, $countRoute = FALSE)
    {
        parent::__construct();
        if ($_SERVER['REQUEST_METHOD'] == 'GET') {
            if ($countRoute == 1 && $route[0] == 'product') {
                $this->index();
            } elseif ($countRoute == 2 && $route[0] == 'product' && $route[1] == 'add') {
                $this->item();
            } elseif ($countRoute == 2 && $route[0] == 'product' && is_numeric($route[1])) {
                $this->item($route[1]);
            } else {
                $this->renderNotFound('main');
                die();
            }
        }
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            if ($countRoute == 2 && $route[0] == 'product' && $route[1] == 'add') {
                $this->AddProduct();
            }elseif ($countRoute == 2 && $route[0] == 'product' && is_numeric($route[1])) {
                $this->UpdatePet($route[1]);
            }
            elseif ($countRoute == 2 && $route[0] == 'product' && $route[1] == 'delete') {
                $this->DeleteProduct();
            }
        }
    }

    private function index(){
        $_oProd = new Prod();
        $aProd = $_oProd->findAll(array('order'=>array('desc'=>'id')));
        $this->result['result']=$aProd;

        $this->renderView("Pages/product","pet",$this->result);
    }
    private function item($id = FALSE){
        if($id){
            $_oProd = new Prod();
            $aProd = $_oProd->findById($id);
            $_oProductType = new Producttype();
            $aProd['prod_type'] = $_oProductType->findByName( array('fild_name'=>'prod_id','fild_val'=>$id));


            $this->result['result']=$aProd;



           // $this->result['prod_type']=$_aProductType;
        }

        $_oPets = new Pets();
        $_oCategories = new Categories();

        $_aPets = $_oPets->findAll(array());
        $this->result['pets']=$_aPets;

        $_aCategories = $_oCategories->findAll(array());
        $this->result['categories']=$_aCategories;
//        echo '<pre>';
//        var_dump($_aCategories);die;

        $this->renderView("Pages/productinfo","productinfo",$this->result);
    }

    private function AddProduct(){

        if(isset($_POST['pet_id'])){
            $pet_id = $_POST['pet_id'];
            unset($_POST['pet_id']);
        }


        if (isset($_FILES['image']) && $_FILES['image']['name'] != '') {
            if ($_FILES['image']['error'] != 0) {
                setcookie("error", "Error Image format", time() + 20, "/");
                $this->sendErrorHeader();
                die();
            }

            $this->objImage = new \ImageTools\Uploads();
            $this->objImage->setFile($_FILES['image']);
            $this->objImage->setImageCurrPath('../assets/images/product/');
            $this->uploadData = $this->objImage->upload();
            if ($this->uploadData['error']) {
                $_POST['image'] = $this->uploadData['data']['img'];
                unset($_POST['real_img']);
            } else {
                $_POST['image'] = $_POST['real_img'];
                unset($_POST['real_img']);

            }
        }
        $_oProd = new Prod();
        $_oProd->_post=$_POST;
        $lastId = $_oProd->insert();

        $_oProductType = new Producttype();
        unset($_POST);
        $myPost['pet_id'] = $pet_id;
        $myPost['prod_id'] = $lastId;
//        echo '<pre>';
//        var_dump($myPost);die;
        for($i = 0; $i <= count($myPost['pet_id']); $i++) {
            $itogPost['prod_id'] = $lastId;
            $itogPost['pet_id'] = $myPost['pet_id'][$i];
            $_oProductType->_post = $itogPost;
            $_oProductType->insert();
        }
        $url = "product";
        $this->headreUrl($url);
    }
    private function UpdatePet($id){
        var_dump($_POST);die;
    }
    private function DeleteProduct(){
        $id = $_POST['id'];
        $_oPartnersModel = new Prod();
        $aPartnersModel = $_oPartnersModel->findById($id);

        $_oProducttype = new Producttype();
        $_aProducttype = $_oProducttype->findByMultyName(array('prod_id'=>$id));


        for($i = 0; $i < count($_aProducttype); $i++){
//            var_dump($_aProducttype[$i]['id']);die;
            $_oProducttype->delFildName = 'id';
            $_oProducttype->delValue = $_aProducttype[$i]['id'];
            $_oProducttype->delete();
        }


        $aImg = $aPartnersModel['image'];
        $filename = '../assets/images/product/'.$aImg;
        unlink($filename);
        $_oPartnersModel->delFildName = 'id';
        $_oPartnersModel->delValue = $id;
        $_oPartnersModel->delete();
        echo json_encode(array('error'=>true));
    }
}