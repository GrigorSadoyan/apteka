<?php
/**
 * Created by PhpStorm.
 * User: sadoy
 * Date: 4/25/2018
 * Time: 2:15 PM
 */

namespace administrator\Controller;
use administrator\Core\Controller as BaseController;
use Model\Pets;
class Pet extends  BaseController
{
    public function __construct($route = FALSE,$countRoute = FALSE)
    {
        parent::__construct();
        if($_SERVER['REQUEST_METHOD'] == 'GET') {
            if ($countRoute == 1 && $route[0] == 'pet') {
                $this->index();
            }elseif ($countRoute == 2 && $route[0] == 'pet' && $route[1] == 'add') {
                $this->item();
            }elseif ($countRoute == 2 && $route[0] == 'pet' && is_numeric($route[1])) {
                $this->item($route[1]);
            }else{
                $this->renderNotFound('main');
                die();
            }
        }
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            if ($countRoute == 2 && $route[0] == 'pet' && $route[1] == 'add'){
                $this->addPet();
            }elseif ($countRoute == 2 && $route[0] == 'pet' && is_numeric($route[1])) {
                $this->UpdatePet($route[1]);
            }elseif ($countRoute == 2 && $route[0] == 'pet' && $route[1] == 'delete') {
                $this->DeletePet();
            }
        }
    }

    private function index(){
        $_oPets = new Pets();
        $aPets = $_oPets->findAll(array('order'=>array('desc'=>'id')));
        $this->result['result']=$aPets;
        $this->renderView("Pages/pet","pet",$this->result);
    }
    private function item($id = FALSE){
        if($id){
            $_oPets = new Pets();
            $aPets = $_oPets->findById($id);
            $this->result['result']=$aPets;
        }
        $this->renderView("Pages/petinfo","petinfo",$this->result);
    }
    private function addPet(){
        $_oPets = new Pets();
        $_oPets->_post=$_POST;
        $lastId = $_oPets->insert();
        $url = "pet";
        $this->headreUrl($url);
    }
    private function UpdatePet($id){
        $_oPets = new Pets();
        $_oPets->_put = $_POST;
        $_oPets->setId($id);
        $_oPets->update();
        $url = "pet";
        $this->headreUrl($url);
    }
    private function DeletePet(){
        $id = $_POST['id'];
        $_oPets = new Pets();
        $_oPets->delFildName = 'id';
        $_oPets->delValue = $id;
        $_oPets->delete();
        echo json_encode(array('error'=>true));
    }
}