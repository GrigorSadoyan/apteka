<div id="menu">
    <div class='user_panel'>
        <div class="pull-left image">
            <img src="<?= $baseurl ?>/a_assets/images/user/1.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
            <p>Apteka</p>
            <a href="#"><i class="fa fa-circle"></i> Online</a>
        </div>
    </div>
    <ul class='sidebar-menu'>
        <li class='header'>NAVIGATION</li>
        <li class="menuli _click_next <?= $page == 'about' || $page == 'blog'  ? "active" : "" ?>">
            <i class="fa fa-book"></i> 
            <span>Main Pages</span>
            <span class='open_sub_menu pull-right'><i class="fa fa-chevron-down"></i></span>
        </li>
        <ul class='main_sub_menu <?= $page == 'about' || $page == 'blog' ? "active_sub" : "" ?>'>
            <li class="header">Pages</li>
            <a href='<?= $baseurl ?>/categories/'>
                <li class='sub_menu <?= $page == 'categories' ? "sub_active'" : "" ?>'>
                    <i class="fa fa-circle-o"></i>
                    <span>Categories</span>
                </li>
            </a>
            <a href='<?= $baseurl ?>/pet/'>
                <li class='sub_menu <?= $page == 'pet' ? "sub_active'" : "" ?>'>
                    <i class="fa fa-circle-o"></i>
                    <span>Kendaniner</span>
                </li>
            </a>
            <a href='<?= $baseurl ?>/product/'>
                <li class='sub_menu <?= $page == 'product' ? "sub_active'" : "" ?>'>
                    <i class="fa fa-circle-o"></i>
                    <span>Product</span>
                </li>
            </a>

            <li class="fotter_menu"></li>
        </ul>
    </ul>
</div>
