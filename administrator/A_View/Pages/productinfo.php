<div id='content'>

    <form id='main_form' action='' method='post' enctype="multipart/form-data">

        <div class='box'>
            <div class='box_header'>
                <h3 class="box-title">Содержание страницы Product</h3>
                <div class="box-tools">
                    <button type="button" class="minresize_box setsize"><i class="fa fa-minus"></i></button>
                </div>
            </div>

            <div class="box_edit box_ck">
                <div class="form_input">
                    <label>Name</label>
                    <div class="input_group">
                        <div class="input_img"><i class="fa fa-pencil"></i></div>
                        <input type="text" class="input_text input_text_home" name='name' placeholder="имя" value='<?= isset($params['result']['name']) ? $params['result']['name'] : '' ?>'>
                    </div>
                </div>
                <div class="form_input">
                    <label>Price</label>
                    <div class="input_group">
                        <div class="input_img"><i class="fa fa-pencil"></i></div>
                        <input type="number" class="input_text input_text_home" name='price' placeholder="price" value='<?= isset($params['result']['price']) ? $params['result']['price'] : '' ?>'>
                    </div>
                </div>

                <div class="form_input">
                    <label>Description</label>
                    <div class="input_group">
                        <div class="input_img"><i class="fa fa-pencil"></i></div>
                        <textarea  class="input_text input_text_home" name='description' cols="100" rows="6"><?= isset($params['result']['description']) ? $params['result']['description'] : '' ?></textarea>
                    </div>
                </div>



                <div class="form_input">
                    <label>Count</label>
                    <div class="input_group">
                        <div class="input_img"><i class="fa fa-pencil"></i></div>
                        <input type="number" class="input_text input_text_home" name='count' placeholder="имя" value='<?= isset($params['result']['count']) ? $params['result']['count'] : '' ?>'>
                    </div>
                </div>

                <div class="form_input">
                    <label><b>Kendanu Tesak</b></label>
                    <div class="input_group">
                        <div>
                            <?php foreach ($params['pets'] as $val){   ?>
                                <label><?=$val['name']?></label>
                                <input name="pet_id[]" type="checkbox" value="<?=$val['id']?>"
                                    <?php
                                    if(isset($params['result']['prod_type'])) {
                                        for ($i = 0; $i <count($params['result']['prod_type']); $i++){
                                            if ($val['id'] == $params['result']['prod_type'][$i]['pet_id']) {
                                                echo 'checked';
                                            }
                                        }
                                    }
                                ?>
                                 >
                            <?php  } ?>
                        </div>
                    </div>
                </div>

                <div class="form_input">
                    <label><b>Categories Tesak</b></label>
                    <div class="input_group">
                        <div>
                            <?php foreach ($params['categories'] as $val){  ?>
                                <label><?=$val['name']?></label>
                                <input name="cat_id" type="checkbox" value="<?=$val['id']?>"
                                    <?php
                                    if(isset($params['result'])) {
                                            if($params['result']['cat_id'] == $val['id']){
                                                echo 'checked';
                                            }
                                        }
                                    ?>
                                >
                            <?php  } ?>
                        </div>
                    </div>
                </div>
                <div class="form_input">
                    <div class="form_input">
                        <label>Upload Galleri image (.jpg,.png)</label>
                        <div class="input_group">
                            <div class='_foto_block foto_block forempty'>
                                <img src='<?=$baseurlM?>/assets/images/product/<?= isset($params['result']['image']) && $params['result']['image'] != '' ? $params['result']['image'] : ''?>' />
                                <input type="file" name="image" class="img_file" >
                                <div class='empty_foto'><i class="fa fa-picture-o"></i></div>
                                <div class='full_foto'></div>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="real_img" value="<?= isset($params['result']['image']) && $params['result']['image'] != '' ? $params['result']['image'] : ''?>" >
                </div>


                <div class="clear"></div>
                <div class="form_input a_form_butt">
                    <div class="input_group clen">
                        <div class="input_img forsave"><i class="fa fa-floppy-o"></i></div>
                        <button class='save' for='main_form'>Сохранить</button>
                    </div>
                </div>
            </div>


        </div>
    </form>
</div>