<?php

namespace Controller;
use Core\Controller as BaseController;

class Product extends BaseController
{
    public function __construct($route = FALSE,$countRoute= FALSE)
    {
        parent::__construct();
        if($_SERVER['REQUEST_METHOD'] == 'GET') {
            if ($countRoute == 2 && $route[0] == 'product' && is_numeric($route[1])) {
                $this->index();
            }else{
                $this->renderNotFound('main');
                die();
            }
        }
    }

    public function index()
    {

        $this->renderView("Pages/product",'product',$this->result);
    }
}