<?php

namespace Controller;
use Core\Controller as BaseController;

class Categories extends BaseController
{
    public function __construct($route = FALSE,$countRoute= FALSE)
    {
        parent::__construct();
        if($_SERVER['REQUEST_METHOD'] == 'GET') {
            if ($countRoute == 2 && $route[0] == 'categories' && is_numeric($route[1])) {
                $this->index($route[1]);
            }elseif($countRoute == 3 && $route[0] == 'categories' && is_numeric($route[1]) && is_numeric(($route[2]))){
                $this->item($route[1],$route[2]);
            }else{
                $this->renderNotFound('main');
                die();
            }
        }
//        if($_SERVER['REQUEST_METHOD'] == 'POST'){
//            if ($countRoute == 2 && $route[0] == 'categories' && is_numeric($route[1])){
//                $this->SendOrder($route[1]);
//            }
//        }
    }
    public function index($id=false){
        $this->renderView("Pages/categories","categories",$this->result);
    }
    private function item($cId = false, $subId = false){
        $this->renderView("Pages/categories","categories",$this->result);
    }
}