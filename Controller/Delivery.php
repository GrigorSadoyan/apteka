<?php

namespace Controller;
use Core\Controller as BaseController;

class Delivery extends BaseController
{
    public function __construct($route = FALSE,$countRoute= FALSE)
    {
        parent::__construct();
        if($_SERVER['REQUEST_METHOD'] == 'GET') {
            if ($countRoute == 1 && $route[0] == 'delivery') {
                $this->index();
            }else{
                $this->renderNotFound('main');
                die();
            }
        }
    }

    public function index()
    {

        $this->renderView("Pages/delivery",'delivery',$this->result);
    }
}