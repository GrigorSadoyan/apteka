<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
</head>
<body style="margin:0;">
<table width="100%" cellspacing="0" cellpadding="0" align="center" style="border-collapse:collapse;background:#f6f6f6;" >
    <tbody>
    <tr>
        <td>
            <table width="608" cellspacing="0" cellpadding="0" align="center" style="border-collapse:collapse;">
                <tbody>
                <tr>
                    <td height="50" colspan="3">
                    </td>
                </tr>
                <tr>
                    <td bgcolor="#ffffff">
                        <table width="100%" cellspacing="0" cellpadding="0" align="center" style="border-collapse:collapse;">
                            <tbody>
                            <tr>
                                <td style="padding:16px 50px 10px 30px;" rowspan="3">
                                    <a target="_blank" title="petlox" href="#">
                                        <img src="<?=$baseurl?>/assets/images/mails/forgot.jpg" width="60" height="57" style="border:none;" alt="forex">
                                    </a>
                                <td style="padding: 10px 30px 0 0; text-align: right;">1(888)695-0009</td>
                                </td>
                            </tr>
                            <tr><td style="padding: 0 30px; text-align: right;">212 N. Glendale Ave Ste 100</td></tr>
                            <tr><td style="padding: 0 30px 10px 0; text-align: right;">Glendale CA, 91206</td></tr>
                            <tr>
                                <td align="center" colspan="2">
                                    <a target="_blank" title="petlox" href="#">
                                        <img width="608" height="230" style="border:none;" alt="forex" src="<?=$baseurl?>/assets/images/mails/forgot.jpg">
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:7px 30px 30px 30px;" colspan="2">
                                    <table width="100%" cellspacing="0" cellpadding="0" align="center" style="border-collapse:collapse;">
                                        <tbody>
                                        <tr>
                                            <td valign="top">
                                                <p style="color:#444444;font-family:Arial;font-size:14px;margin-bottom:15px;margin-top:20px;line-height:19px;">
                                                    Hi, <b><?=$params['l_name']?> <?=$params['f_name']?></b>.
                                                </p>
                                                <p style="color:#444444;font-family:Arial;font-size:15px;margin-top:5px;margin-bottom:20px;line-height:20px;">
                                                    <a href="<?=$params['generate']?>">Chenge password</a>
                                                </p>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" colspan="2">
                                    <table width="100%" cellspacing="0" cellpadding="0" align="center" style="border-collapse:collapse;">
                                        <tbody>
                                        <tr>
                                            <td width="200" align="left" style="padding:0px 0px 20px 30px;">
                                                <span style="color:#999999;font-family:Arial;font-size:14px;line-height:19px;">Petlox Team</span>
                                            </td>
                                            <td width="200" align="right" style="padding:0px 30px 20px 0px;">
                                                <div style="color:#222222;font-family:Arial;font-size:14px;line-height:19px;margin-bottom:4px;">
                                                    <a style="margin-right:2px;" target="_blank" href="#"><img width="18" height="18" alt="fb" style="border:none;" src="<?=$baseurl?>/assets/images/mails/registartion/facebook.png"></a>
                                                    <a style="margin-right:2px;" target="_blank" href="#"><img width="18" height="18" alt="tw" style="border:none;" src="<?=$baseurl?>/assets/images/mails/registartion/twitter.png"></a>
                                                    <a style="margin-right:2px;" target="_blank" href="#"><img width="18" height="18" alt="vk" style="border:none;" src="<?=$baseurl?>/assets/images/mails/registartion/google_plus.png"></a>
                                                </div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <table width="100%" cellspacing="0" cellpadding="0" style="border-collapse:collapse;margin-top:20px;">
                            <tbody>
                            <tr>
                                <td width="270" align="left" style="padding:0px 0px 20px 30px;vertical-align:bottom;">
                                    <div style="color:#222222;font-family:Arial;font-size:13px;line-height:18px;">
                                        <a style="color:#666699;text-decoration:none;" target="_blank" href="#">Support</a>&nbsp; &nbsp; &nbsp; &nbsp; <a style="color:#666699;text-decoration:none;" target="_blank" href="#">Contact us</a>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="50" colspan="3">
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>
</body>
</html>