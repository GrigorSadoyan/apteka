<footer>
    <div class="col-md-12  col-xs-12 main_header ">
        <div class="content clear">
            <div class="col-md-12 col-xs-12 header clear nopad">
                <div class="col-md-12 col-xs-12 main_footer nopad">
                    <div class="col-md-4 col-xs-4 main_footer_left">
                        <i class="fa fa-location-arrow"></i>
                        <span>Բագրատունյաց 2-րդ նրբ. , N3 շին.</span></br>
                        <i class="fa fa-clock-o"></i>
                        <span> Երկ. - կիրակի ժ 8:00 - 22:00</span></br>
                    </div>
                    <div class="col-md-4 col-xs-4 main_footer_middle">
                        <i class="fa fa-phone"></i>
                        <span>  <a href="tel:+37499077005">+374(99)077005</a></span></br>
                        <i class="fa fa-envelope-o"></i>
                        <span>  info@agrovet.com</span></br>
                    </div>
                    <div class="col-md-4 col-xs-4 main_footer_right">
                        <img src="<?=$baseurl?>/assets/images/content/icons/google.png">
                        <img src="<?=$baseurl?>/assets/images/content/icons/fb.png">
                        <img src="<?=$baseurl?>/assets/images/content/icons/instagram.png">
                        <a href="#top"><img src="<?=$baseurl?>/assets/images/content/icons/back-to-top-.png"></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
</body>
</html>
