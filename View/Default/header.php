<!doctype html>
<html lang="en">
<head>
    <title>Apteka</title>
    <link rel="icon" href="<?= $baseurl ?>/assets/images/content/fav_logo.png" type="image/png">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="<?= $baseurl ?>/assets/javascript/lib/jquery.fancybox/source/jquery.fancybox.css">
    <link rel="stylesheet" href="<?= $baseurl ?>/assets/css/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="<?= $baseurl ?>/assets/css/bootstrap/css/bootstrap-theme.css">
    <link rel="stylesheet" type="text/css" href="<?= $baseurl ?>/assets/css/slick/slick.css">
    <link rel="stylesheet" type="text/css" href="<?= $baseurl ?>/assets/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?= $baseurl ?>/assets/css/style.css">
    <script type="text/javascript"><?php echo "var base = '".$baseurl."';"; ?></script>
    <script src="<?= $baseurl ?>/assets/javascript/jquery2.2.4.min.js"></script>
    <script src="<?= $baseurl ?>/assets/css/bootstrap/js/bootstrap.js"></script>
    <script type="text/javascript" src="<?= $baseurl ?>/assets/javascript/lib/jquery.fancybox/source/jquery.fancybox.pack.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js"></script>
    <script src="<?= $baseurl ?>/assets/javascript/script.js"></script>
</head>
<body>
<header>
    <div class="col-md-12  col-xs-12 main_header ">
        <div class="content clear">
            <div class="col-md-12 col-xs-12 header clear nopad" id="top" >
                <div class="col-md-12 col-xs-12 blue nopad">
                    <div  class="blueleft clear">
                        <a href="<?=$baseurl?>/about/">
                            <div class="tat">
                                <i class="fa fa-paw"></i><span class="saasdasdasd">Մեր մասին</span>
                            </div>
                        </a>
                        <a href="<?=$baseurl?>/contact/">
                            <div class="tat">
                                <i class="fa fa-paw"></i><span class="saasdasdasd">Կոնտակտներ</span>
                            </div>
                        </a>
                        <a href="<?=$baseurl?>/delivery/">
                            <div class="tat">
                                <i class="fa fa-paw"></i><span class="saasdasdasd">Առաքում</span>
                            </div>
                        </a>
                        <!-- <div class="blueright">
                            <div class="signin">

                                <i class="fa fa-sign-in"></i>
                                <span  data-toggle="modal" data-target="#myModal">Մուտք</span>


                            </div>
                            <div class="register">
                                <i class="fa fa-user-plus"></i>
                                <span   data-toggle="modal" data-target="#myModal1">Գրանցում</span>
                            </div>
                        </div>
                        -->
                    </div>
                </div>
                <div  class="col-md-12 col-xs-12 underheader nopad clear">
                    <a href="<?=$baseurl?>/">
                        <div class="col-md-3 col-xs-3 logo nopad">
                            <img src="<?=$baseurl?>/assets/images/content/logo.png" >
                        </div>
                    </a>
                    <div class="col-md-6 col-xs-6 serch nopad">
                        <input type="text" name="search" placeholder="Փնտրում եմ">
                        <button class="serchbtn">Որոնել</button>
                        <div class="example">
                            Օրինակ , <a href=""> Albendazol-600</a>
                        </div>
                    </div>
                    <div class="col-md-3 col-xs-3 basket nopad">
                        <a href="<?=$baseurl?>/cart/">
                            <div>
                                <button class="basketbtn"><i class="fa fa-shopping-basket" aria-hidden="true"></i>Զամբյուղ<span>(0)</span></button>
                            </div>
                        </a>
                        <div class="col-md-12 col-xs-12 total_price nopad">
                            Գումար : <span><b>0</b></span><b> դր.</b>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-xs-12 main_menu nopad">

                            <?php foreach ($params['pets'] as $pet) { ?>
                                <ul class="main_ul">
                                    <li class="main_menu_g"><a href="<?=$baseurl?>/categories/1/"><span><?=$pet['name']?></span></a></li>
                                    <li class="main_hov_li">
                                        <div>
                                            <p><a href="<?=$baseurl?>/categories/1/2/">Դեղորայք</a></p>
                                            <p><a href="<?=$baseurl?>/categories/1/2/">Կերեր</a></p>
                                            <p><a href="<?=$baseurl?>/categories/1/2/">Խնամք</a></p>
                                            <p><a href="<?=$baseurl?>/categories/1/2/">Այլ</a></p>

                                        </div>
                                    </li>
                                </ul>
                            <?php   } ?>

                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Մուտք</h4>
                    <h7 class="modal-title">Մուտքագրեք Ձեր էլ. հասցեն և գաղտնաբառը</h7>
                </div>
                <div class="modal-body">
                    <form>
                        <div>
                            <input type="email" class="modal_log_input" placeholder="Էլ. հասցե" required >
                        </div>
                        <div>
                            <input type="text" class="modal_log_input" placeholder="գաղտնաբառ" required>
                        </div>
                        <button type="button" class="modal_button">Մուտք</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="myModal1" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Գրանցում</h4>
                    <h7 class="modal-title">Մուտքագրեք Ձեր տվյալները համապատասխան դաշտերում</h7>
                </div>
                <div class="modal-body">
                    <form>
                        <div>
                            <input type="text" class="modal_log_input" placeholder="Անուն" required>
                        </div>
                        <div>
                            <input type="text" class="modal_log_input" placeholder="Ազգանուն" required>
                        </div>
                        <div>
                            <input type="text" class="modal_log_input" placeholder="Հեռախոսահամար" required>
                        </div>
                        <div>
                            <input type="email" class="modal_log_input" placeholder="Էլ. հասցե" required >
                        </div>
                        <div>
                            <input type="password" class="modal_log_input" placeholder="Գաղտնաբառ" required>
                        </div>
                        <div>
                            <input type="password" class="modal_log_input" placeholder="Կրկնել գաղտնաբառը" required>
                        </div>
                        <button type="button" class="modal_button">Մուտք</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6 col-xs-6 error_div">
        <p class="error_text">
            Ձեր էլ. հասցեն կամ գաղտնաբառը սխալ է: Խնդրում ենք մուտքագրել ճիշտ:
        </p>
    </div>
    <div class="col-md-6 col-xs-6 error_div">
        <p class="error_text_2">
            Տվյալ էլ. հասցեով գրանցված բաժանորդ արդեն իսկ կա: Խնդրում ենք մուտքագրել ճիշտ էլ. հասցե:
        </p>
    </div>
</header>