<section>
    <div class="content clear">
        <div class="col-md-12 col-xs-12 sliderdiv clear">
            <div class="col-md-4 col-xs-4 slide_text">
                <p>Կյանքը հիասքանչ է եթե կանք մենք  </p>
            </div>
            <div class=" col-md-10 col-xs-10 middle_main">
                <div class=" middle ">
                    <div class="col-md-8 col-xs-8">
                        <div class="myimg">
                            <img src="<?=$baseurl?>/assets/images/content/animal_group.jpg">
                        </div>
                    </div>
                </div>
                <div class=" middle ">
                    <div class="col-md-8 col-xs-8">
                        <div class="myimg">
                            <img src="<?=$baseurl?>/assets/images/content/qweasd.png">
                        </div>
                    </div>
                </div>
                <div class=" middle ">
                    <div class="col-md-8 col-xs-8">
                        <div class="myimg">
                            <img src="<?=$baseurl?>/assets/images/content/animal_group.jpg">
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <script>
        $(document).ready(function () {
            $('.middle_main').slick({
                prevArrow: '<div class="prew_sl">\n' +
                '            <i class="fa fa-chevron-left " aria-hidden="true"></i>\n' +
                '        </div>',
                nextArrow: '<div class="next_sl">\n' +
                '            <i class="fa fa-chevron-right" aria-hidden="true"></i>\n' +
                '        </div>',
                autoplay: true,
                autoplaySpeed: 5000,
                fade:true
            });
        })
    </script>
    <div class="nopad under_slider">
        <div class="content   pad nopad clear">
            <div class="for_padding">
                <?php
                $ds = DIRECTORY_SEPARATOR;
                $base_dir = realpath(dirname(__FILE__)  . $ds . '..') . $ds;
                require_once("{$base_dir}Pages{$ds}include{$ds}menu.php");
                ?>
                <div class="col-md-9 col-xs-9">
                    <p class="about_apteka">Բարի գալուստ<span> « </span><span>Agrovet.am</span><span> » </span></p>
                    <p class="about_text">    Դեղատունը գործում է 2018 թվականից: Այն զբաղվում է անասնաբուժական դեղորայքի, կենսապատրաստուկների,
                        ախտահանիչների, վիրաբուժական գործիքների, սարքավորումների, մասնագիտական գրականության, վիտամինահանքային միացությունների,
                        կերային հավելումների, կերերի, կենդանիների խնամքի պարագաների  առևտրով:
                    </p>
                    <p class="about_text">    Դեղատանը  յուրաքանչյուր հաճախորդի նկատմանբ
                        ցուցաբերվում է անհատական մոտեցում, իրականացվում է բարձրակարգ մասնագիտական խորհրդատվություն, ինչպես նաև, գործում է
                        գնային ճկուն քաղաքականություն:
                    </p>
                    <p class="top_products">Թեժ վաճառք</p>
                    <a href="<?=$baseurl?>/product/1/">
                        <div class="col-md-3 col-xs-3 product_main nopad">
                            <div class="about_product">
                                <div class="img_helper_main">
                                    <div class="img_helper">
                                        <img src="<?=$baseurl?>/assets/images/content/brovanol_plus.jpg">
                                    </div>
                                    <div class="prod_description">
                                        <div>
                                            <b class="h1">Անուն</b>
                                            <span class="description">դեղի մասին համառոտ տեղեկութ- յուն կամ բնութագրություն</span>
                                        </div>
                                    </div>
                                    <div class="product_total_price">
                                        <span class="actual">1000</span>
                                        <span class="actual_2">դր.</span>
                                        <button class="add_to_card">ավելացնել</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                    <a href="<?=$baseurl?>/product/1/">
                        <div class="col-md-3 col-xs-3 product_main nopad">
                            <div class="about_product">
                                <div class="img_helper_main">
                                    <div class="img_helper">
                                        <img src="<?=$baseurl?>/assets/images/content/pill.png">
                                    </div>
                                    <div class="prod_description">
                                        <div>
                                            <b class="h1">Անուն</b>
                                            <span class="description">դեղի մասին համառոտ տեղեկութ- յուն կամ բնութագրություն</span>
                                        </div>
                                    </div>
                                    <div class="product_total_price">
                                        <span class="actual">1000</span>
                                        <span class="actual_2">դր.</span>
                                        <button class="add_to_card">ավելացնել</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                    <a href="<?=$baseurl?>/product/1/">
                        <div class="col-md-3 col-xs-3 product_main nopad">
                            <div class="about_product">
                                <div class="img_helper_main">
                                    <div class="img_helper">
                                        <img src="<?=$baseurl?>/assets/images/content/pill.png">
                                    </div>
                                    <div class="prod_description">
                                        <div>
                                            <b class="h1">Անուն</b>
                                            <span class="description">դեղի մասին համառոտ տեղեկութ- յուն կամ բնութագրություն</span>
                                        </div>
                                    </div>
                                    <div class="product_total_price">
                                        <span class="actual">1000</span>
                                        <span class="actual_2">դր.</span>
                                        <button class="add_to_card">ավելացնել</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                    <a href="<?=$baseurl?>/product/1/">
                        <div class="col-md-3 col-xs-3 product_main nopad">
                            <div class="about_product">
                                <div class="img_helper_main">
                                    <div class="img_helper">
                                        <img src="<?=$baseurl?>/assets/images/content/pill.png">
                                    </div>
                                    <div class="prod_description">
                                        <div>
                                            <b class="h1">Անուն</b>
                                            <span class="description">դեղի մասին համառոտ տեղեկութ- յուն կամ բնութագրություն</span>
                                        </div>
                                    </div>
                                    <div class="product_total_price">
                                        <span class="actual">1000</span>
                                        <span class="actual_2">դր.</span>
                                        <button class="add_to_card">ավելացնել</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                    <a href="prod.php">
                        <div class="col-md-3 col-xs-3 product_main nopad">
                            <div class="about_product">
                                <div class="img_helper_main">
                                    <div class="img_helper">
                                        <img src="<?=$baseurl?>/assets/images/content/pill.png">
                                    </div>
                                    <div class="prod_description">
                                        <div>
                                            <b class="h1">Անուն</b>
                                            <span class="description">դեղի մասին համառոտ տեղեկութ- յուն կամ բնութագրություն</span>
                                        </div>
                                    </div>
                                    <div class="product_total_price">
                                        <span class="actual">1000</span>
                                        <span class="actual_2">դր.</span>
                                        <button class="add_to_card">ավելացնել</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                    <a href="prod.php">
                        <div class="col-md-3 col-xs-3 product_main nopad">
                            <div class="about_product">
                                <div class="img_helper_main">
                                    <div class="img_helper">
                                        <img src="<?=$baseurl?>/assets/images/content/pill.png">
                                    </div>
                                    <div class="prod_description">
                                        <div>
                                            <b class="h1">Անուն</b>
                                            <span class="description">դեղի մասին համառոտ տեղեկութ- յուն կամ բնութագրություն</span>
                                        </div>
                                    </div>
                                    <div class="product_total_price">
                                        <span class="actual">1000</span>
                                        <span class="actual_2">դր.</span>
                                        <button class="add_to_card">ավելացնել</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                    <a href="prod.php">
                        <div class="col-md-3 col-xs-3 product_main nopad">
                            <div class="about_product">
                                <div class="img_helper_main">
                                    <div class="img_helper">
                                        <img src="<?=$baseurl?>/assets/images/content/pill.png">
                                    </div>
                                    <div class="prod_description">
                                        <div>
                                            <b class="h1">Անուն</b>
                                            <span class="description">դեղի մասին համառոտ տեղեկութ- յուն կամ բնութագրություն</span>
                                        </div>
                                    </div>
                                    <div class="product_total_price">
                                        <span class="actual">1000</span>
                                        <span class="actual_2">դր.</span>
                                        <button class="add_to_card">ավելացնել</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                    <a href="prod.php">
                        <div class="col-md-3 col-xs-3 product_main nopad">
                            <div class="about_product">
                                <div class="img_helper_main">
                                    <div class="img_helper">
                                        <img src="<?=$baseurl?>/assets/images/content/pill.png">
                                    </div>
                                    <div class="prod_description">
                                        <div>
                                            <b class="h1">Անուն</b>
                                            <span class="description">դեղի մասին համառոտ տեղեկութ- յուն կամ բնութագրություն</span>
                                        </div>
                                    </div>
                                    <div class="product_total_price">
                                        <span class="actual">1000</span>
                                        <span class="actual_2">դր.</span>
                                        <button class="add_to_card">ավելացնել</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 col-xs-12 adventures nopad">
        <div class="clear">
            <div class="col-md-3 col-xs-3 section">
                <div class="col-md-3 col-xs-3 rightNopad section_img">
                    <img src="<?=$baseurl?>/assets/images/content/icons/med_bottle.png">
                </div>
                <div class="col-md-9 col-xs-9 section_header ">
                    <p class="section_header_title">Հարուստ տեսականի</p>
                </div>
                <div class="col-md-12 col-xs-12">
                    <p class="section_header_text">դեղատանն առկա է դեղերի, խնամքի պարագաների, մասնագիտական գրականության
                        և այլ ապրանքների լայն տեսականի </p>
                </div>
            </div>
            <div class="col-md-3 col-xs-3 section">
                <div class="col-md-3 col-xs-3 rightNopad section_img">
                    <img src="<?=$baseurl?>/assets/images/content/icons/klass.png">
                </div>
                <div class="col-md-9 col-xs-9 section_header ">
                    <p class="section_header_title">Բարձր որակ</p>
                </div>
                <div class="col-md-12 col-xs-12">
                    <p class="section_header_text">դեղատունը վաճառվում է միայն որակյալ ապրանքներ և հատուկ ուշադրություն է
                        դարձվում պահպանման ժամկետներին</p>
                </div>
            </div>
            <div class="col-md-3 col-xs-3 section">
                <div class="col-md-3 col-xs-3 rightNopad section_img">
                    <img src="<?=$baseurl?>/assets/images/content/icons/tolk.png">
                </div>
                <div class="col-md-9 col-xs-9 section_header ">
                    <p class="section_header_title">Խորհրդատվություն</p>
                </div>
                <div class="col-md-12 col-xs-12">
                    <p class="section_header_text">դեղատանն հաճախորդի նկատմամբ ցուցաբերվում է անհատական մոտեցում
                        և տրամադրվում է անվճար խորհրդատվություն</p>
                </div>
            </div>
            <div class="col-md-3 col-xs-3 section">
                <div class="col-md-3 col-xs-3 rightNopad section_img">
                    <img src="<?=$baseurl?>/assets/images/content/icons/fast_delivery.png">
                </div>
                <div class="col-md-9 col-xs-9 section_header ">
                    <p class="section_header_title">Արագ առաքում</p>
                </div>
                <div class="col-md-12 col-xs-12">
                    <p class="section_header_text">դեղատան նաև Երևանի տարածում իրականացնում է առաքում մատչելի գներով և
                        հնարավորինս սեղմ ժամկետներում
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>