<div class="col-md-12  col-xs-12 main_header ">
    <div class="content clear">
        <div class="center_content">
            <div class="col-md-12  col-xs-12 contact_main">
                <p class="map_title">Քարտեզ</p>
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4134.395507299324!2d44.476074609502334!3d40.155117512090555!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zNDDCsDA5JzE4LjUiTiA0NMKwMjgnNDEuOCJF!5e0!3m2!1sru!2s!4v1521793879464" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
            <div class="col-md-8 col-xs-8 contact_main ">
                <div class="col-md-3 col-xs-3 contact_right">
                    <p>Հասցե:</p></br>
                    <p>Հեռախոս:</p></br>

                </div>
                <div class="col-md-9 col-xs-9 contact_left">
                    <p>Բագրատունյաց փ. 2-րդ նրբանցք, համար 3 շինություն</p></br>
                    <p>+374 099 07-70-05</p></br>
                    <p>+374 098 46-41-49</p></br>
                    <p>+374 055 46-41-49</p></br>
                </div>
            </div>
            <div class="col-md-4  col-xs-4 contact_main">
                <p class="contact_title">Հետադարձ կապ</p>
                <form>
                    <input type="text" placeholder="ԱՆՈՒՆ" required></br>
                    <input type="email" placeholder="ԷԼ. ՀԱՍՑԵ" required>
                    <textarea rows="4" cols="45" placeholder="ՀԱՂՈՐԴԱԳՐՈՒԹՅԱՆ ՏԵՔՍՏԸ" required></textarea>
                    <button>ՈՒՂՂԱՐԿԵԼ ՆԱՄԱԿԸ</button>
                </form>
            </div>
        </div>
    </div>
</div>