<div class="col-md-12 col-xs-12 main_prod_div clear">
<div class="content clear">
    <?php
    $ds = DIRECTORY_SEPARATOR;
    $base_dir = realpath(dirname(__FILE__)  . $ds . '..') . $ds;
    require_once("include{$ds}menu.php");
    ?>
<div class="col-md-9 col-xs-9 prod_margin clear">
    <div class="col-md-12 col-xs-12 prod_div">
        <div class="col-md4 col-xs-4 prod_div_img">
            <img src="<?=$baseurl?>/assets/images/content/brovanol_plus.jpg">
        </div>
        <div class="col-md-8 col-xs-8 prod_div_desc clear">
            <b>Անուն</b>
            <span class="description">
                դեղի մասին համառոտ տեղեկություն կամ բնութագրություն
                արտադրող  երկիր , ֆիրմա, ինչի համար է նախատեսված
            </span>
        </div>
        <div class="col-md-8 col-xs-8 prod_div_price">
            <span class="prod_price">1000</span>
            <span class="prod_price_aqount">դր.</span>
            <button class="add_to_card">ավելացնել</button>
        </div>
        <div class="col-md-12 col-xs-12 under_prod_div">
            <p>նկարագրությունը</p>
            <div>
                <p class="bold">
                    Описание:
                </p>
                <p class="unbold">
                    Таблетки плоскиеб цилиндрические со скошенными краями, с насечкой для деления на
                    одной стороне и логотипом производителя на другой, белого или серовато-белого цвета со слабым
                    характерным запахом.
                </p>
                <div>
                    <div>
                        <p class="bold">
                            Дозирование:
                        </p>
                        <p class="unbold">
                            Одна таблетка на 10 кг массы тела. Таблетки измельчают и смешивают с третью
                            кормовой массы утреннего рациона. Для молодых животных в возрасте до 1,5 года указанная доза
                            делится на две части и скармливается с суточным интервалом. При наличии демодекоза
                            Брованол-плюс дается повторно через 10-12 дней. Одновременно применяются препараты для
                            симптоматической терапии.
                            Если у животных отсутствуют кожные паразиты, то для профилактической дегельминтизации следует
                            применять препарат Брованол Д.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
