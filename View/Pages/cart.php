<div class="col-md-12  col-xs-12 main_header ">
    <div class="content clear">
        <div class="col-md-12  col-xs-12 my_basket">
            <p>Իմ Զամբյուղը</p>
        </div>
        <div class="col-md-12 col-xs-12 my_basket_content clear nopad">
            <div class="main_card clear">
                <div class="col-md-8  col-xs-12 center">
                    <table class="table table-hover text-center">
                        <thead>
                        <tr>
                            <th class="col-md-5 col-xs-2">Ապրանք</th>
                            <th class="col-md-5 col-xs-2">Քանակ</th>
                            <th class="col-md-5 col-xs-2">Գին</th>
                            <th class="col-md-5 col-xs-2">Գումարը</th>
                            <th class="col-md-5 col-xs-2">Ջնջել</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr class="_countprod_">
                            <td class="col-xs-3">
                                <div class="col-md-3 col-xs-5">
                                    <img src="">
                                </div>
                                <div class="col-md-9 col-xs-8">Բրովանոլ + ճիճվամուղ</div>
                            </td>
                            <td>
                                <div class="col-md-9 col-xs-12">
                                    <input type="number" id="oioioi" class="form-control data_count" value="1">
                                </div>
                                <div class="col-md-2 nopad">
                                    <div class="nopad  _count_" data-count="+">
                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                    </div>
                                    <div class="nopad  _count_" data-count="-">
                                        <i class="fa fa-minus" aria-hidden="true"></i>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <span class="gin">1000</span>
                            </td>
                            <td  class="one_total">
                            </td>
                            <td>
                                 <span class="del_from_card">
                                     <i class="fa fa-trash fa-2x" aria-hidden="true"></i>
                                 </span>
                            </td>
                        </tr>
                        <tr class="_countprod_">
                            <td class="col-xs-3">
                                <div class="col-md-3 col-xs-5">
                                    <img src="">
                                </div>
                                <div class="col-md-9 col-xs-8">Բրովանոլ + ճիճվամուղ</div>
                            </td>
                            <td>
                                <div class="col-md-9 col-xs-12">
                                    <input type="number" class="form-control data_count" value="1">
                                </div>
                                <div class="col-md-2 nopad">
                                    <div class="nopad  _count_" data-count="+">
                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                    </div>
                                    <div class="nopad  _count_" data-count="-">
                                        <i class="fa fa-minus" aria-hidden="true"></i>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <span class="gin">1000</span>
                            </td>
                            <td  class="one_total">

                            </td>
                            <td>
                                 <span class="del_from_card">
                                     <i class="fa fa-trash fa-2x" aria-hidden="true"></i>
                                 </span>
                            </td>
                        </tr>

                        <tr class="_countprod_">
                            <td class="col-xs-3">
                                <div class="col-md-3 col-xs-5">
                                    <img src="">
                                </div>
                                <div class="col-md-9 col-xs-8">Բրովանոլ + ճիճվամուղ</div>
                            </td>
                            <td>
                                <div class="col-md-9 col-xs-12">
                                    <input type="text" class="form-control data_count" value="1">
                                </div>
                                <div class="col-md-2 nopad">
                                    <div class="nopad  _count_" data-count="+">
                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                    </div>
                                    <div class="nopad  _count_" data-count="-">
                                        <i class="fa fa-minus" aria-hidden="true"></i>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <span class="gin">1000</span>
                            </td>
                            <td  class="one_total">
                            </td>
                            <td>
                                 <span class="del_from_card">
                                     <i class="fa fa-trash fa-2x" aria-hidden="true"></i>
                                 </span>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <div class="total_price clear">
                        <div class="right ">Ընդհանուր գումարը<span class="price_span">x</span></div>
                    </div>
                    <div class="payment_box clear">

                        <button class="order_button right">Պատվերն ուղղարկել</button>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(".form-control").keyup(function(){
        var ImInp = $(this);
        var ImInpVal = $(this).val();
        var OnePrice = ImInp.parent('div').parent('td').next('td').text();
        //console.log(OnePrice);
        var total = ImInpVal * OnePrice;
        //console.log(total);
        ImInp.parent('div').parent('td').next('td').next('td').text(total);


        var allOTotals = $(".one_total");
        console.log(allOTotals);
        var opshi_price = 0;
        for(var i = 0; i <allOTotals.length ; i++){
            var ttt = parseInt(allOTotals[i].innerText);
            opshi_price += ttt;
            //console.log(typeof  ttt);
            //console.log(ttt);
            //console.log(opshi_price);
        }
        $('.price_span').text(opshi_price);
    });

    $(document).ready(function(){
        $('._count_').click(function(){
            var znak = $(this).attr('data-count');

            var imInpVal = $(this).parent('div').prev('div').children('input').val();
            var imInpVals = parseInt(imInpVal);

            if(imInpVals > 0 ){
                if(znak == '-'){
                    if(imInpVals == 1){
                        alert('no');
                        return false;
                    }else{
                        imInpVals -= 1;
                        console.log(imInpVals);
                    }
                }

                if(znak == '+'){
                    imInpVals += 1;
                }
                console.log(imInpVals);
                $(this).parent('div').prev('div').children('input').val(imInpVals);

                // stex kanchelu enq en function@ vor@ ej@ baceluc piti opshi hashvi
            }else{
                return false;
            }

        })





        var allInputs = $(".form-control");
        //console.log(allInputs);
        var opshi_price = 0;
        for(var i = 0; i <allInputs.length ; i++){
            var thisCount = allInputs[i].value;
            //console.log(allInputs[i]);
            var OnePrice = $(allInputs[i]).parent('td').next('.gin').text();
            var verjOneGin =  $(allInputs[i]).parent('td').next('td').next('td');
            var yyy = thisCount * OnePrice;
            var ttt = verjOneGin.text(yyy);
            opshi_price += yyy;
        }
        $('.price_span').text(opshi_price);
    });
</script>